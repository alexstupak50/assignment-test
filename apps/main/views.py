#coding: utf-8
from django.views.generic import ListView, DetailView

from apps.main.models import Post


class Index(ListView):
    template_name = "main/main_content.html"
    model = Post

    def get_queryset(self):
        if self.request.user.is_superuser:
            return self.model.objects.all()
        else:
            return self.model.objects.filter(is_published=True)


class PostDetailView(DetailView):
    model = Post

#coding: utf-8
from django.contrib import admin

from apps.main.models import Post


class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'author')


admin.site.register(Post, PostAdmin)

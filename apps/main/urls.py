# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url

from apps.main.views import Index, PostDetailView


urlpatterns = patterns('',
    url(r'^$', Index.as_view(), name='index'),
    url(r'^post/(?P<pk>\d+)/$', PostDetailView.as_view(), name='post_detail'),
)

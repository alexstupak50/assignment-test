#coding: utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse


class Post(models.Model):
    title = models.CharField(_(u'Название'), max_length=255)
    pub_date = models.DateTimeField(_(u'Дата публикации'), auto_now_add=True)
    content = models.TextField(_(u'Контент'), max_length=10000)
    author = models.CharField(_(u'Автор'), max_length=255)
    is_published = models.BooleanField(_(u'Опубликованая'), default=False)

    def __unicode__(self):
        return u'%s - %s' % (self.author, self.title)

    def get_absolute_url(self):
        return reverse('main:post_detail', kwargs={'pk': self.id})
